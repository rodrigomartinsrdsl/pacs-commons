package br.com.rdsl.InfoMed.StateManagement;

import org.bson.Document;
import org.bson.types.ObjectId;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.UpdateOptions;

import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Updates.*;

import br.com.rdsl.InfoMed.DicomModel.WorkList;

public class WorkListStateManager {

	public static void RegisterStatusChange(WorkList wl , WorkListStatus wlStatus, boolean saveStatusAtDataBase) {
		System.out.println("Wl An: " + wl.getAN() + " new status: " + wlStatus);
		
		if(saveStatusAtDataBase == true)
			changeWorkListStatusAtMongoDB(wl, wlStatus);
	}
	
	private static void changeWorkListStatusAtMongoDB(WorkList wl, WorkListStatus wlStatus) {
		
		MongoClient mongoClient = new MongoClient();
		MongoDatabase database = mongoClient.getDatabase("PacsCentral");
		MongoCollection<Document> collection = database.getCollection("WorkLists");
		System.out.println("Wl Id String " + wl.getId().toString() + " status " + wlStatus.getCode());
		collection.updateOne(
				eq("_id", new ObjectId(wl.getId().toString())),
				combine(set("status", wlStatus.getCode()) , currentDate("lastModified")),
				new UpdateOptions().upsert(true).bypassDocumentValidation(true));
		
		mongoClient.close();
	}
}
