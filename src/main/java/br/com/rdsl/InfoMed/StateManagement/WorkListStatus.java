package br.com.rdsl.InfoMed.StateManagement;

public enum WorkListStatus {
	
	REGISTRADO_CENTRAL(1),
	//RECEBIDO_UNIDADE("Recebido Pacs Unidade"),
	REGISTRADO_UNIDADE(2),
	PRONTO_LAUDAR(3),
	SINCRONIZADO_CENTRAL(4);
	
	private int _statusText;
	
	private WorkListStatus(int statusText) {
		this._statusText = statusText;
	}
	
	public int getCode() {
		return this._statusText;
	}
}
