package br.com.rdsl.InfoMed.DicomModel;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.IndexOptions;
import org.mongodb.morphia.annotations.Indexed;

@Entity("WorkLists")

public class WorkList {
	
	@Id
	private ObjectId id;
	
	@Indexed(options= @IndexOptions(unique=true))
	private String AN;
	private String StudyInstanceUID;
	@Embedded
	private Paciente paciente;
	@Embedded
	private Solicitante solicitante;
	@Embedded
	private Unidade unidade;
	@Embedded
	private Procedimento procedimento;
	private String modalidade;
	private String dataAtendimento;
	private String horaAtendimento;
	private String medicalAlerts;
	private String allergies;
	
	private Date timeStamp = new Date();
	
	private int status;
	
	public Date getTimeStamp() {
		return timeStamp;
	}
	public String getMedicalAlerts() {
		return medicalAlerts;
	}
	public void setMedicalAlerts(String medicalAlerts) {
		this.medicalAlerts = medicalAlerts;
	}
	public String getAllergies() {
		return allergies;
	}
	public void setAllergies(String allergies) {
		this.allergies = allergies;
	}
	
	public String getAN() {
		return AN;
	}
	public Paciente getPaciente() {
		return paciente;
	}
	public Solicitante getSolicitante() {
		return solicitante;
	}
	public Unidade getUnidade() {
		return unidade;
	}
	public String getModalidade() {
		return modalidade;
	}
	
	public Procedimento getProcedimento() {
		return procedimento;
	}
	public void setProcedimento(Procedimento procedimento) {
		this.procedimento = procedimento;
	}
	public String getDataAtendimento() {
		return dataAtendimento;
	}
	
	public void setAN(String aN) {
		AN = aN;
	}
	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}
	public void setSolicitante(Solicitante solicitante) {
		this.solicitante = solicitante;
	}
	public void setUnidade(Unidade unidade) {
		this.unidade = unidade;
	}
	public void setModalidade(String modalidade) {
		this.modalidade = modalidade;
	}
	
	public void setDataAtendimento(String dataAtendimento) {
		this.dataAtendimento = dataAtendimento;
	}
	public Boolean isvalid() {
		return this.unidade != null &&
				this.paciente != null &&
				this.solicitante != null && 
				this.modalidade != null &&
				this.procedimento != null &&
				this.AN != null ;
			
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((AN == null) ? 0 : AN.hashCode());
		result = prime * result + ((modalidade == null) ? 0 : modalidade.hashCode());
		result = prime * result + ((paciente == null) ? 0 : paciente.hashCode());
		result = prime * result + ((procedimento == null) ? 0 : procedimento.hashCode());
		result = prime * result + ((solicitante == null) ? 0 : solicitante.hashCode());
		result = prime * result + ((unidade == null) ? 0 : unidade.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WorkList other = (WorkList) obj;
		if (AN == null) {
			if (other.AN != null)
				return false;
		} else if (!AN.equals(other.AN))
			return false;
		if (modalidade == null) {
			if (other.modalidade != null)
				return false;
		} else if (!modalidade.equals(other.modalidade))
			return false;
		if (paciente == null) {
			if (other.paciente != null)
				return false;
		} else if (!paciente.equals(other.paciente))
			return false;
		if (procedimento == null) {
			if (other.procedimento != null)
				return false;
		} else if (!procedimento.equals(other.procedimento))
			return false;
		if (solicitante == null) {
			if (other.solicitante != null)
				return false;
		} else if (!solicitante.equals(other.solicitante))
			return false;
		if (unidade == null) {
			if (other.unidade != null)
				return false;
		} else if (!unidade.equals(other.unidade))
			return false;
		return true;
	}
	public String getStudyInstanceUID() {
		return StudyInstanceUID;
	}
	public void setStudyInstanceUID(String studyInstanceUID) {
		StudyInstanceUID = studyInstanceUID;
	}
	public String getHoraAtendimento() {
		return horaAtendimento;
	}
	public void setHoraAtendimento(String horaAtendimento) {
		this.horaAtendimento = horaAtendimento;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public ObjectId getId() {
		return id;
	}
	public void setId(ObjectId id) {
		this.id = id;
	}
	
	
}
