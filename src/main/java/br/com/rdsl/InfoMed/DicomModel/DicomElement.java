package br.com.rdsl.InfoMed.DicomModel;

public class DicomElement {
	String ImageType;
	String SOPClassUID;
	String SOPInstanceUID;
	String StudyDate;
	String SeriesDate;
	String AcquisitionDate;
	String ContentDate;
	String StudyTime;
//	....
	String AccessionNumber;
//	...
	String PatientName;
	String PatientID;
//	...
	String StudyInstanceUID;
	String SeriesInstanceUID;
	String StudyID;
	String SeriesNumber;
	public String getImageType() {
		return ImageType;
	}
	public void setImageType(String imageType) {
		ImageType = imageType;
	}
	public String getSOPClassUID() {
		return SOPClassUID;
	}
	public void setSOPClassUID(String sOPClassUID) {
		SOPClassUID = sOPClassUID;
	}
	public String getSOPInstanceUID() {
		return SOPInstanceUID;
	}
	public void setSOPInstanceUID(String sOPInstanceUID) {
		SOPInstanceUID = sOPInstanceUID;
	}
	public String getStudyDate() {
		return StudyDate;
	}
	public void setStudyDate(String studyDate) {
		StudyDate = studyDate;
	}
	public String getSeriesDate() {
		return SeriesDate;
	}
	public void setSeriesDate(String seriesDate) {
		SeriesDate = seriesDate;
	}
	public String getAcquisitionDate() {
		return AcquisitionDate;
	}
	public void setAcquisitionDate(String acquisitionDate) {
		AcquisitionDate = acquisitionDate;
	}
	public String getContentDate() {
		return ContentDate;
	}
	public void setContentDate(String contentDate) {
		ContentDate = contentDate;
	}
	public String getStudyTime() {
		return StudyTime;
	}
	public void setStudyTime(String studyTime) {
		StudyTime = studyTime;
	}
	public String getAccessionNumber() {
		return AccessionNumber;
	}
	public void setAccessionNumber(String accessionNumber) {
		AccessionNumber = accessionNumber;
	}
	public String getPatientName() {
		return PatientName;
	}
	public void setPatientName(String patientName) {
		PatientName = patientName;
	}
	public String getPatientID() {
		return PatientID;
	}
	public void setPatientID(String patientID) {
		PatientID = patientID;
	}
	public String getStudyInstanceUID() {
		return StudyInstanceUID;
	}
	public void setStudyInstanceUID(String studyInstanceUID) {
		StudyInstanceUID = studyInstanceUID;
	}
	public String getSeriesInstanceUID() {
		return SeriesInstanceUID;
	}
	public void setSeriesInstanceUID(String seriesInstanceUID) {
		SeriesInstanceUID = seriesInstanceUID;
	}
	public String getStudyID() {
		return StudyID;
	}
	public void setStudyID(String studyID) {
		StudyID = studyID;
	}
	
	public String getSeriesNumber() {
		return SeriesNumber;
	}
	public void setSeriesNumber(String seriesNumber) {
		SeriesNumber = seriesNumber;
	}
	
	@Override
	public String toString() {
		return "DicomElement [ImageType=" + ImageType + ", SOPClassUID=" + SOPClassUID + ", SOPInstanceUID="
				+ SOPInstanceUID + ", StudyDate=" + StudyDate + ", SeriesDate=" + SeriesDate + ", AcquisitionDate="
				+ AcquisitionDate + ", ContentDate=" + ContentDate + ", StudyTime=" + StudyTime + ", AccessionNumber="
				+ AccessionNumber + ", PatientName=" + PatientName + ", PatientID=" + PatientID + ", StudyInstanceUID="
				+ StudyInstanceUID + ", SeriesInstanceUID=" + SeriesInstanceUID + ", StudyID=" + StudyID
				+ ", SeriesNumber=" + SeriesNumber + "]";
	}
	
}
