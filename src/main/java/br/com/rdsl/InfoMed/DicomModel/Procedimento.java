package br.com.rdsl.InfoMed.DicomModel;

public class Procedimento {
	
	private String nome;
	private String codigo;
	public Procedimento() {}
	public Procedimento(String nome, String codigo) {
		super();
		this.nome = nome;
		this.codigo = codigo;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
