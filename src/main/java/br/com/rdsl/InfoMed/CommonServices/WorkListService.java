package br.com.rdsl.InfoMed.CommonServices;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Arrays;
import java.util.List;

import com.google.gson.Gson;

import br.com.rdsl.InfoMed.DicomModel.WorkList;

public class WorkListService {
	public static List<WorkList> parseWorkListJsonToObj(InputStream inputStream) {
		Gson gson = new Gson();
		Reader reader = new InputStreamReader(inputStream);
		WorkList[] arr = gson.fromJson(reader, WorkList[].class);
		return Arrays.asList(arr);
	}
}
