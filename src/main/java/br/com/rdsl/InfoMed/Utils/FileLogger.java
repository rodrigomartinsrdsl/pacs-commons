package br.com.rdsl.InfoMed.Utils;

import org.pmw.tinylog.Logger;

public class FileLogger {
	public static void writeWARN(String data) {
		Logger.warn(data);
	}
	
	public static void writeINFO(String data) {
		Logger.info(data);
	}
}
