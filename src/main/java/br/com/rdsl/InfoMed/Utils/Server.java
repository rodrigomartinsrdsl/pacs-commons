package br.com.rdsl.InfoMed.Utils;

public class Server {
	private String port;
	private String address;
	private String AETName;

	public String getPort() {
		return port;
	}

	public String getAddress() {
		return address;
	}

	public Server(String AETName, String address, String port) {
		this.address = address;
		this.port = port;
		this.AETName = AETName;
	}

	@Override
	public String toString() {
		return "Server [address=" + address + "], [port=" + port + "], [AET="+ AETName +"]";
	}

	public String getAETName() {
		
		return this.AETName;
	}
	
	
}
