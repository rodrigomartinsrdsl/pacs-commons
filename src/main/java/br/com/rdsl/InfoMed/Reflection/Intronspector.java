package br.com.rdsl.InfoMed.Reflection;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class Intronspector {
	public static Map<String, Object> getNotNullFiels(Object obj){
		Map<String, Object> notNullFields = new HashMap<>();
		 for (Field field : obj.getClass().getDeclaredFields()) {
		        //field.setAccessible(true); // if you want to modify private fields
		        try {
		        	field.setAccessible(true);
		        	String fieldName = field.getName();
		        	String fieldType = field.getType().toString();
		        	Object fieldValue = field.get(obj);
		        	if(fieldType.toLowerCase().contains("map"))
		        		if(((Map<?, ?>) fieldValue).size() ==  0) fieldValue = null;
		        	if(fieldValue != null)
		        		notNullFields.put(fieldName, fieldValue);
		        	field.setAccessible(false);
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    }
		 return notNullFields;
	}
}
