package br.com.rdsl.InfoMed.DicomServices;

import java.io.File;
import java.util.Properties;

public class DicomConfig {
	//protected final static String DCMTK_MODIFY_BIN = "/usr/local/bin/dcmtk-3.6.0/bin/dcmodify ";
	//protected final static String DCMTK_DCM2XML_BIN = "/usr/local/bin/dcmtk-3.6.0/bin/dcm2xml ";
	
	private String DCMTK_BIN_LOCATION;
	
	public DicomConfig(Properties props) {
		DCMTK_BIN_LOCATION =  props.getProperty("DCMTK_BIN_LOCATION");
	}
	
	public String getDCMTK_MODIFY_BIN() {
		return DCMTK_BIN_LOCATION + File.separator + "dcmodify ";
	}
	
	public String getDCMTK_DCM2XML_BIN() {
		return DCMTK_BIN_LOCATION + File.separator + "dcm2xml ";
	}

	public String getDCMTK_DUMP2DCM_BIN() {
		return DCMTK_BIN_LOCATION + File.separator + "dump2dcm ";
	}

	
	public String getDCMTK_STORESCU_BIN() {
		return DCMTK_BIN_LOCATION + File.separator + "storescu ";
	}
	
	public String getDCMTK_FINDSCU_BIN() {
		return DCMTK_BIN_LOCATION + File.separator + "findscu ";
	}
}
