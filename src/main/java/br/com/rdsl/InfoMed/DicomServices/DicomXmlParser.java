package br.com.rdsl.InfoMed.DicomServices;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import br.com.rdsl.InfoMed.DicomModel.DicomElement;

public class DicomXmlParser {
	public static DicomElement parse(String xmlFile, Properties props) throws Exception {
		//byte[] bytes = xmlFile.getBytes(StandardCharsets.ISO_8859_1);
		//InputStream stream = new ByteArrayInputStream(bytes);
		
		if(xmlFile.contains(".dcm")) {
			Dicom2XML dicomXML = new Dicom2XML(xmlFile, props); //is not a XML it's a DICOM so Transfor
			dicomXML.generate();
			xmlFile = dicomXML.getGeneratedXMLFile();
		}
		
		DicomElement dicomElement = new DicomElement();
		
		try {

			File fXmlFile = new File(xmlFile);
			if(fXmlFile.exists() == false) throw new FileNotFoundException(xmlFile);
	        DocumentBuilderFactory dbFactory2 = DocumentBuilderFactory.newInstance();
	        DocumentBuilder dBuilder2 = dbFactory2.newDocumentBuilder();
			Document doc = dBuilder2.parse(fXmlFile);
			fXmlFile.delete();
			doc.getDocumentElement().normalize();
			
			//System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

			NodeList nList = doc.getElementsByTagName("data-set");
			//int l = nList.getLength();
			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);

				//System.out.println("\nCurrent Element :" + nNode.getNodeName());

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;

					//System.out.println("name: " + eElement.getAttribute("name"));
					NodeList nodeList = eElement.getChildNodes();
					for (int count = 0; count < nodeList.getLength(); count++) {

						Node tempNode = nodeList.item(count);

						// make sure it's element node.
						if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
							String nodeName = tempNode.getNodeName();
							
							if(nodeName == "element") {
								dicomElement = procressElementNode(tempNode, dicomElement);
							}
							
						}
					}
					
					//System.out.println(dicomElement);
					
				}
			}

		} catch (ParserConfigurationException | SAXException | IOException e) {
			// TODO Auto-generated catch block
			throw new Exception(e.getMessage());
		}
		
		return dicomElement; 
	}
	
	private static DicomElement procressElementNode(Node tempNode,DicomElement dicomElement){
		//DicomElement dicomElement = new DicomElement();
		
		String valorDoNode = tempNode.getTextContent();
		
		if (tempNode.hasAttributes()) {

			// get attributes names and values
			NamedNodeMap nodeMap = tempNode.getAttributes();
			
			for (int i = 0; i < nodeMap.getLength(); i++) {

				Node node = nodeMap.item(i);
				String attrName = node.getNodeName();
				String attrvalue = node.getNodeValue();
				
				//System.out.println("attr name : " + attrName);
				//System.out.println("attr value : " + attrvalue);
				if(attrName == "name"){
					if(attrvalue.equalsIgnoreCase("ImageType")) dicomElement.setImageType(valorDoNode);
					if(attrvalue.equalsIgnoreCase("SOPClassUID")) dicomElement.setSOPClassUID(valorDoNode);
					if(attrvalue.equalsIgnoreCase("SOPInstanceUID")) dicomElement.setSOPInstanceUID(valorDoNode);
					if(attrvalue.equalsIgnoreCase("StudyDate")) dicomElement.setStudyDate(valorDoNode);
					if(attrvalue.equalsIgnoreCase("SeriesDate")) dicomElement.setSeriesDate(valorDoNode);
					if(attrvalue.equalsIgnoreCase("AcquisitionDate")) dicomElement.setAcquisitionDate(valorDoNode);
					if(attrvalue.equalsIgnoreCase("ContentDate")) dicomElement.setContentDate(valorDoNode);
					if(attrvalue.equalsIgnoreCase("StudyTime")) dicomElement.setStudyTime(valorDoNode);
					if(attrvalue.equalsIgnoreCase("AccessionNumber")) dicomElement.setAccessionNumber(valorDoNode);
					if(attrvalue.equalsIgnoreCase("PatientName")) dicomElement.setPatientName(valorDoNode);
					if(attrvalue.equalsIgnoreCase("PatientID")) dicomElement.setPatientID(valorDoNode);
					if(attrvalue.equalsIgnoreCase("StudyInstanceUID")) dicomElement.setStudyInstanceUID(valorDoNode);
					if(attrvalue.equalsIgnoreCase("SeriesInstanceUID")) dicomElement.setSeriesInstanceUID(valorDoNode);
					if(attrvalue.equalsIgnoreCase("StudyID")) dicomElement.setStudyID(valorDoNode);
					if(attrvalue.equalsIgnoreCase("SeriesNumber")) dicomElement.setSeriesNumber(valorDoNode);
					continue;
				}
			}
		}
		return dicomElement;
	}
}
