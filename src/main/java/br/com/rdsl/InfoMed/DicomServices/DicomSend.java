package br.com.rdsl.InfoMed.DicomServices;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.PumpStreamHandler;

import br.com.rdsl.InfoMed.Utils.*;

public class DicomSend {

	private DicomConfig DicomConfig;

	public DicomSend() {
		Properties props = new Properties();

		try {
			BufferedReader reader = Files.newBufferedReader(Paths.get("DicomSend.properties"));
			props.load(reader);

		} catch (IOException e1) {
			System.err.println("Problema na leitura do Arquivo de Properties: " + e1.getMessage());
		}
		this.DicomConfig = new DicomConfig(props);
	}

	public DicomSend(Properties props) {
		this.DicomConfig = new DicomConfig(props);
	}

	public void send(Path dicomFile, Server server) throws Exception {
		send(dicomFile.toString(), server);
	}
	public void send(String dicomFile, Server server) throws Exception {
		StringBuffer sb = new StringBuffer();

		sb.append(DicomConfig.getDCMTK_STORESCU_BIN() + " -xv -xw -aec " + server.getAETName() + " "
				+ server.getAddress() + " " + server.getPort() + " ");
		sb.append(dicomFile);

		CommandLine cmdLine = null;
		if (OSValidator.isWindows()) {
			cmdLine = new CommandLine("cmd.exe");

			cmdLine.addArgument("/c");
		} else {
			cmdLine = new CommandLine("/bin/sh");
			cmdLine.addArgument("-c");
		}

		cmdLine.addArgument(sb.toString(), false);

		//System.out.println(cmdLine.toString());

		DefaultExecutor executor = new DefaultExecutor();
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		PumpStreamHandler streamHandler = new PumpStreamHandler(outputStream);
		executor.setStreamHandler(streamHandler);

		executor.execute(cmdLine);
		String out = outputStream.toString();
		if (out.isEmpty() == false)
			throw new Exception("File not send: " + dicomFile);

	}
}
