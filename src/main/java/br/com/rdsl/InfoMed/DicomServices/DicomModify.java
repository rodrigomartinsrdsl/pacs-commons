package br.com.rdsl.InfoMed.DicomServices;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import static java.nio.file.StandardCopyOption.*;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.apache.commons.exec.PumpStreamHandler;
import org.xml.sax.SAXException;

import br.com.rdsl.InfoMed.DicomModel.DicomElement;
import br.com.rdsl.InfoMed.Reflection.Intronspector;
import br.com.rdsl.InfoMed.Utils.OSValidator;

@SuppressWarnings("all")
public class DicomModify {
	
	private String StudyDate;
	private String SeriesDate;
	private String StudyTime;
	private String SeriesTime;
	private String AccessionNumber;
	private String InstitutionName;
	private String PatientID;
	private String PatientName;
	private String StudyInstanceUID;
	private String StudyID;
	private String SeriesInstanceUID;
	private String StudyDescription;
	private String PatientBirthDate;
	private String PatientSex;
	private String RequestingPhysician;
	private String dicomFile;
	private Map<String, String> TagsToChange = new HashMap<>();
	private Map<String, String> dicomTagsLookup = new HashMap<>();
	private DicomElement dicomElement;
	private DicomConfig DicomConfig;

	public DicomModify() {}
	
	public DicomModify(String dicomFile, Properties props, Boolean... shouldNotParse) throws Exception{
		this.dicomFile = dicomFile;
		 //default to "false"
	    boolean shouldParseFlag = (shouldNotParse.length >= 1) ? true : false;
	    
	    if(shouldParseFlag == false) {
	    	try {
				this.dicomElement  = DicomXmlParser.parse(this.dicomFile, props);
			} catch (FileNotFoundException e) {
				
				throw new RuntimeException(e.getMessage());
				
			} catch (SAXException e) {
				throw new RuntimeException(e.getMessage());
			}
	    }
		
		dicomTagsLookup.put("StudyDate", "(0008,0020)");
		dicomTagsLookup.put("SeriesDate", "(0008,0021)");
		dicomTagsLookup.put("StudyTime", "0008,0030");
		dicomTagsLookup.put("SeriesTime", "(0008,0031)");
		
		dicomTagsLookup.put("AccessionNumber", "(0008,0050)");
		dicomTagsLookup.put("InstitutionName", "(0008,0080)");
		dicomTagsLookup.put("PatientID", "(0010,0020)");
		dicomTagsLookup.put("StudyInstanceUID", "(0020,000d)");
		dicomTagsLookup.put("StudyID", "(0020,0010)");
		dicomTagsLookup.put("SeriesInstanceUID", "(0020,000e)");
		dicomTagsLookup.put("PatientName", "(0010,0010)");
		dicomTagsLookup.put("StudyDescription", "(0008,1030)");
		
		dicomTagsLookup.put("PatientBirthDate", "(0010,0030)");
		dicomTagsLookup.put("PatientSex", "(0010,0040)");
		dicomTagsLookup.put("RequestingPhysician", "(0032,1032)");
		
		
		this.DicomConfig = new DicomConfig(props);
	}
	
	public File executeChange(Map<String,Object> dicomTagsToChange) {
		StringBuffer sb = new StringBuffer();
		try {
			sb.append(DicomConfig.getDCMTK_MODIFY_BIN());
			int sucessDicomTagsLookup = 0;
			for (Map.Entry<String, Object> mapElement : dicomTagsToChange.entrySet()) {
				String dicomTagCodeFound = dicomTagsLookup.get(mapElement.getKey());
				if(dicomTagCodeFound != null && mapElement.getValue() != null ) {
					sb.append(" -i \""+ dicomTagCodeFound + "=" + mapElement.getValue().toString().trim() + "\" ");
					sucessDicomTagsLookup++;
				}
			}
			sb.append(this.dicomFile);
			DefaultExecutor executor = new DefaultExecutor();
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		    PumpStreamHandler streamHandler = new PumpStreamHandler(outputStream);
		    executor.setStreamHandler(streamHandler);
		    
			if(sucessDicomTagsLookup >  0) {
				CommandLine cmdLine = null;
				if(OSValidator.isWindows()){
					cmdLine = new CommandLine("cmd.exe");
					
					cmdLine.addArgument("/c");
				}else{
					cmdLine = new CommandLine("/bin/sh");
					cmdLine.addArgument("-c");
				}
				cmdLine.addArgument(sb.toString(), false);
				
				//System.out.println(cmdLine.toString());
				
				executor.execute(cmdLine);
				
				String out = outputStream.toString();
				
				File backupFile = new File(this.dicomFile + ".bak");
				
				if(backupFile .exists() == false) {
					throw new RuntimeException("Backup Dicom File not generated at Modifying process, Verify !");
				}
				
				String filePath = new File(this.dicomFile).getParent();
				
				File changedFolder = new File(filePath + File.separator + "changed");
				
				boolean isDirectoryPresent = changedFolder.exists();
				
				if(!isDirectoryPresent) changedFolder.mkdir();
				
				File destinationFile = new File(filePath + File.separator  + "changed" + File.separator + new File(this.dicomFile).getName());
				
				Files.move(new File(this.dicomFile).toPath(), destinationFile.toPath() , REPLACE_EXISTING);
				
				backupFile.renameTo(new File(this.dicomFile));
				
				//new File("/path/to/folder").mkdir();
				
				return destinationFile;
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
//		try {
//			TimeUnit.MILLISECONDS.sleep(200);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
		
		return null;
	}
	

	public Map<String, Object> getChangedDicomFields(String unidade, String dateNow , String timeNow) {
//		String dateNow = new SimpleDateFormat("YYYYMMdd").format(new Date());
//		String timeNow = new SimpleDateFormat("HHmmss").format(new Date());
		
		String _unidade = unidade;
		String newStudyInstanceUID = this.generateStudyInstanceUID(_unidade, dateNow + timeNow, dicomElement.getAccessionNumber());
		
		String[] ss = dicomElement.getSeriesInstanceUID().split("\\.");
		String lastPieceSeriesInstanceUID = ss[ss.length-1];
		
		this.setStudyDate(dateNow);
		this.setSeriesDate(dateNow);
		
		this.setStudyTime(timeNow);
		this.setSeriesTime(timeNow);
		
		this.setAccessionNumber(_unidade + "." + dicomElement.getAccessionNumber());
		this.setInstitutionName("Hospital " + _unidade);
		this.setPatientID(_unidade + "." + dicomElement.getPatientID());
		
		this.setStudyID(_unidade + "." + dicomElement.getStudyID());
		
		this.setStudyInstanceUID(newStudyInstanceUID);
		
		this.setSeriesInstanceUID(newStudyInstanceUID + "." + lastPieceSeriesInstanceUID);
		
		Map<String, Object> fields = Intronspector.getNotNullFiels(this);
		
		return fields;
	}
	
	private String generateStudyInstanceUID(String idUnidade, String dateTime, String accessionNumber) {
		//Study Instance UID = 1.2. + ID Unidade + "." + DataHoraCorrente(AAAAMMDDHHMMSS) + "." + AN_ORIGINAL *Tem que ser Unico
		StringBuilder sb = new StringBuilder();
		sb.append("1.2.");
		sb.append(idUnidade +  ".");
		sb.append(dateTime  +  ".");
		sb.append(accessionNumber );
		return sb.toString();
	}
	public void setPatientName(String pacientName) {
		this.PatientName = pacientName;
	}
	public void setStudyDate(String studyDate) {
		this.StudyDate = studyDate;
	}

	public void setStudyTime(String studyTime) {
		this.StudyTime = studyTime;
	}

	public void setSeriesDate(String seriesDate) {
		this.SeriesDate = seriesDate;
	}

	public void setSeriesTime(String seriesTime) {
		this.SeriesTime = seriesTime;
	}

	public void setAccessionNumber(String accessionNumber) {
		this.AccessionNumber = accessionNumber;
	}

	public void setInstitutionName(String institutionName) {
		this.InstitutionName = institutionName;
	}

	public void setPatientID(String patientID) {
		this.PatientID = patientID;
	}

	public void setStudyInstanceUID(String studyInstanceUID) {
		this.StudyInstanceUID = studyInstanceUID;
	}

	public void setStudyID(String studyID) {
		this.StudyID = studyID;
	}

	public void setSeriesInstanceUID(String seriesInstanceUID) {
		this.SeriesInstanceUID = seriesInstanceUID;
	}

	public static Map<String, Object> getChanges(String filePathAbsolute , String idUnidade, String dateNow, String timeNow,  Properties props) throws Exception {

		DicomModify dicomModify = new DicomModify(filePathAbsolute, props);
		
		Map<String, Object> fields = dicomModify.getChangedDicomFields(idUnidade , dateNow , timeNow);
		
		return fields;
	}

	public static File applyChanges(String filePathAbsolute, Map<String, Object> changesToBeApplied, Properties props ) throws Exception {
		DicomModify dicomModify = new DicomModify(filePathAbsolute, props);
		return dicomModify.executeChange(changesToBeApplied);
	}
	
	public static File applyChangesWithoutParseDicomFile(String filePathAbsolute, Map<String, Object> changesToBeApplied, Properties props ) throws Exception {
		DicomModify dicomModify = new DicomModify(filePathAbsolute, props, false);
		return dicomModify.executeChange(changesToBeApplied);
	}

	public void setStudyDescription(String studyDescription) {
		StudyDescription = studyDescription;
	}

	public void setPatientBirthDate(String patientBirthDate) {
		PatientBirthDate = patientBirthDate;
	}

	public void setPatientSex(String patientSex) {
		PatientSex = patientSex;
	}

	public void setRequestingPhysician(String requestingPhysician) {
		RequestingPhysician = requestingPhysician;
	}
	
	
}
