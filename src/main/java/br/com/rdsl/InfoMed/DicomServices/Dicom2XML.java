package br.com.rdsl.InfoMed.DicomServices;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.exec.CommandLine; 
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.PumpStreamHandler;

import br.com.rdsl.InfoMed.Utils.OSValidator;

public class Dicom2XML {
	private String dicomFile;
	private String generatedXMLFile;
	
	private DicomConfig DicomConfig;
	public Dicom2XML(String dicomfile, Properties props) {
		this.dicomFile = dicomfile;
		
		this.DicomConfig = new DicomConfig(props);
	}
	public String generate() throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append(DicomConfig.getDCMTK_DCM2XML_BIN());
		sb.append(this.dicomFile);
		File file = new File(this.dicomFile);
		String fileName = file.getName();
		fileName = fileName.replace(".dcm", ".xml");
		String filePath = file.getParent();
		fileName = filePath + File.separator + fileName;
		sb.append(" > "+ fileName);
		
		//CommandLine cmdLine = new CommandLine("cmd.exe");
		//CommandLine cmdLine = new CommandLine("/bin/sh");
		
		CommandLine cmdLine = null;
		if(OSValidator.isWindows()){
			cmdLine = new CommandLine("cmd.exe");
			cmdLine.addArgument("/c");
		}else{
			cmdLine = new CommandLine("/bin/sh");
			cmdLine.addArgument("-c");
		}
        
		cmdLine.addArgument(sb.toString(), false);
		
		//System.out.println(cmdLine.toString());	
		DefaultExecutor executor = new DefaultExecutor();
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
	    PumpStreamHandler streamHandler = new PumpStreamHandler(outputStream);
	    executor.setStreamHandler(streamHandler);
	    
		try {
			executor.execute(cmdLine);
			String out = outputStream.toString();
			if(out.contains("dcm2xml: (Invalid stream)")) throw new Exception("dcm2xml was not able to generate xml file: " + fileName);
			if(out.contains("dcm2xml: (I/O suspension or premature end of stream)")) throw new Exception("dcm2xml was not able to generate xml file: " + fileName);
			if(out.contains("dcm2xml: (End of stream)")) throw new Exception("dcm2xml was not able to generate xml file: " + fileName);
			if(new File(fileName).exists() == false) throw new Exception("dcm2xml was not able to generate xml file: " + fileName);
			this.generatedXMLFile = fileName;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			throw new Exception(e);
		}
		return fileName;
	}
	public String getGeneratedXMLFile() {
		return generatedXMLFile;
	}
}
