package br.com.rdsl.InfoMed.DicomServices;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.PumpStreamHandler;

import br.com.rdsl.InfoMed.DicomModel.Paciente;
import br.com.rdsl.InfoMed.DicomModel.Procedimento;
import br.com.rdsl.InfoMed.DicomModel.Solicitante;
import br.com.rdsl.InfoMed.DicomModel.Unidade;
import br.com.rdsl.InfoMed.DicomModel.WorkList;
import br.com.rdsl.InfoMed.Utils.OSValidator;

public class DicomWorkListQuery {

	private String serverAddress;
	private String serverPort;

	// private DicomConfig DicomConfig;
	private String DCMTK_DOCKER_FINDSCU = "";
	private String AET;
	private String idUndidade;

	public DicomWorkListQuery(Properties props, String serverAddress, String serverPort, String AET,
			String idUndidade) {
		this.serverAddress = serverAddress;
		this.serverPort = serverPort;
		this.AET = AET;
		this.idUndidade = idUndidade;
		String DCMTK_DOCKER_CONTAINER = props.getProperty("DCMTK_DOCKER_CONTAINER");
		DCMTK_DOCKER_FINDSCU = DCMTK_DOCKER_CONTAINER + " findscu ";

	}

	public DicomWorkListQuery(Properties props, String serverAddress, String serverPort) {
		// this.props = props;
		this.serverAddress = serverAddress;
		this.serverPort = serverPort;
		this.AET = "ORTHANC";
		// this.DicomConfig = new DicomConfig(props);

		String DCMTK_DOCKER_CONTAINER = props.getProperty("DCMTK_DOCKER_CONTAINER");
		DCMTK_DOCKER_FINDSCU = DCMTK_DOCKER_CONTAINER + " findscu ";
	}

	public String queryServer() throws IOException {
		CommandLine cmdLine = null;
		if (OSValidator.isWindows()) {
			cmdLine = new CommandLine("cmd.exe");
			cmdLine.addArgument("/c");
		} else {
			cmdLine = new CommandLine("/bin/sh");
			cmdLine.addArgument("-c");
		}
		StringBuffer sb = new StringBuffer();
		// sb.append(DicomConfig.getDCMTK_FINDSCU_BIN());
		sb.append(DCMTK_DOCKER_FINDSCU);
		// k \"ScheduledProcedureStepSequence[0].Modality=CT\"
		String commandProperties = "-W -aet \"findscu\" -aec \"" + this.AET + "\" "
				+ "-k \"ScheduledProcedureStepSequence[0].Modality\" " + "-k 08,50 -k 10,10 -k 10,20 -k 10,30 -k 10,40 "
				+ "-k \"StudyInstanceUID\" " + "-k \"RequestingPhysician\" "
				+ "-k \"ScheduledProcedureStepSequence[0].ScheduledProcedureStepStartDate\" "
				+ "-k \"ScheduledProcedureStepSequence[0].ScheduledProcedureStepStartTime\" "
				+ "-k \"ScheduledProcedureStepSequence[0].ScheduledProcedureStepDescription\" " + "-k 32,1060 "
				+ "-k 10,2000 " + "-k 10,2110 " + "-k \"ScheduledProcedureStepSequence[0].ScheduledStationAETitle\"";

		sb.append(commandProperties);
		sb.append(" " + this.serverAddress);
		sb.append(" " + this.serverPort);
		System.out.println(sb.toString());

		cmdLine.addArgument(sb.toString(), false);
		// cmdLine.addArgument("/tmp/runDockerFindSCU.sh 10.25.133.36 4242",
		// false);
		DefaultExecutor executor = new DefaultExecutor();
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		PumpStreamHandler streamHandler = new PumpStreamHandler(outputStream);
		executor.setStreamHandler(streamHandler);

		executor.execute(cmdLine);
		String out = outputStream.toString();
		// System.out.println(out);
		return out;
	}

	public Map<Integer, List<String>> processFileToBlocks(String fileString) {
		String[] retLines = fileString.split("\n");
		Map<Integer, List<String>> brokenFile = new HashMap<>();
		int blockCounter = 1;
		List<String> listOfLines = new ArrayList<>();

		if (fileString.isEmpty())
			return brokenFile;

		for (int i = 0; i < retLines.length; i++) {
			String line = retLines[i];
			if (i == 0 && (line.contains("W: -------") || line.contains("I: -------")))
				continue;

			if (!(line.contains("W: -------") || line.contains("I: -------"))) {
				listOfLines.add(line);
			} else {
				brokenFile.put(blockCounter, listOfLines);
				blockCounter++;
				// printMapValues(listOfLines);
				// listOfLines.clear();
				listOfLines = new ArrayList<>();
			}
		}
		if (listOfLines != null && listOfLines.size() > 0) {
			// Inclua a ultima lista de Linhas que foi processada
			brokenFile.put(blockCounter, listOfLines);
		}
		return brokenFile;
	}

	public List<WorkList> processBlocksToWorkListObject(Map<Integer, List<String>> blocks) {
		// String[] keys = new String[] {"W: Find Response:", "W: #
		// Dicom-Data-Set", "W: # Used TransferSyntax: Little Endian Explicit"};
		List<WorkList> lwl = new ArrayList<>();
		blocks.forEach((k, v) -> {
			WorkList wl = new WorkList();
			Paciente p = new Paciente();
			Solicitante s = new Solicitante();
			Procedimento procedimento = new Procedimento();
			v.forEach(item -> {
				String justTheValue = extracValueFromLine(item);
				if (item.contains("0008,0050"))
					wl.setAN(justTheValue);
				if (item.contains("0010,0010")) {
					p.nome = justTheValue;
					wl.setPaciente(p);
				}
				if (item.contains("0010,0020")) {
					p.codigo = justTheValue;
					wl.setPaciente(p);
				}
				if (item.contains("0010,0030")) {
					p.dataNascimento = justTheValue;
					wl.setPaciente(p);
				}
				if (item.contains("0010,0040")) {
					p.sexo = justTheValue;
					wl.setPaciente(p);
				}
				if (item.contains("0020,000d")) {
					String[] StudyInstanceUIDCharArray = justTheValue.split("");
					StudyInstanceUIDCharArray = Arrays.stream(StudyInstanceUIDCharArray).filter(l -> {
						Character c = l.charAt(0);
						int hascode = c.hashCode();
						if (hascode > 0) { // Retira caracteres em nulos ou
											// invalidos da string
							return true;
						}
						return false;

					}).toArray(String[]::new);

					String StudyInstanceUIDCleanedWithNotNull = String.join("", StudyInstanceUIDCharArray);
					wl.setStudyInstanceUID(StudyInstanceUIDCleanedWithNotNull);
				}
				if (item.contains("0032,1032")) {
					s.nome = justTheValue;
					wl.setSolicitante(s);
				}
				if (item.contains("0008,0060"))
					wl.setModalidade(justTheValue);
				if (item.contains("0040,0002"))
					wl.setDataAtendimento(justTheValue);
				if (item.contains("0040,0003"))
					wl.setHoraAtendimento(justTheValue);
				if (item.contains("0010,2000"))
					wl.setMedicalAlerts(justTheValue);
				if (item.contains("0010,2110"))
					wl.setAllergies(justTheValue);
				if (item.contains("0032,1060")) {
					// procedimento.setCodigo("<sem codigo>");
					procedimento.setNome(justTheValue);
					wl.setProcedimento(procedimento);
				}
			});
			Unidade u = new Unidade();
			u.idUnidade = extractUnidadeFromAN(wl.getAN());
			if (u.idUnidade.isEmpty())
				u.idUnidade = this.idUndidade; // caso o AN do WorkList nao
												// venja na conversao
												// <<idUnidade.AN>>
			wl.setUnidade(u);
			lwl.add(wl);
		});
		return lwl;
	}

	private String extractUnidadeFromAN(String AN) {
		String[] anArr = AN.split("\\.");
		String unidade = "";
		if (anArr.length > 1) {
			unidade = anArr[0];
		}
		return unidade;
	}

	private String extracValueFromLine(String line) {
		String value = "";
		String[] lineArr = line.split("\\[");
		if (lineArr.length < 2)
			return "";
		value = lineArr[1];
		lineArr = value.split("\\]");
		if (lineArr.length < 2)
			return "";
		value = lineArr[0];
		return value;
	}

	@SuppressWarnings("unused")
	private static void printMapValues(List<String> list) {
		for (String l : list) {
			System.out.println(l);
		}
		System.out.println("---------------------------------------------------");
	}
}
