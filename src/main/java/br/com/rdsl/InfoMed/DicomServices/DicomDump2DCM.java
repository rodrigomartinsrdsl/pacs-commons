package br.com.rdsl.InfoMed.DicomServices;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.apache.commons.exec.PumpStreamHandler;

import br.com.rdsl.InfoMed.Exceptions.WorkListNotGeneratedException;
import br.com.rdsl.InfoMed.Utils.OSValidator;

public class DicomDump2DCM {
	private DicomConfig DicomConfig;
	//private Properties props;
	private String DCMTK_DOCKER_CONTAINER = null;

	public DicomDump2DCM(Properties props) {
		//this.props = props;
		this.DicomConfig = new DicomConfig(props);
		DCMTK_DOCKER_CONTAINER = props.getProperty("DCMTK_DOCKER_CONTAINER");
	}

	public void convert(String dcmFile, String destinationFile) throws WorkListNotGeneratedException, ExecuteException, IOException {
		StringBuffer sb = new StringBuffer();
		dcmFile=  dcmFile.replaceAll(" ", "\\ ");
		File f = new File(dcmFile);
		Path p = f.toPath();
		dcmFile = p.toString().replaceAll(" ", "\\\\ ");
		dcmFile = dcmFile.replaceAll("'", "\\\\'");
		
		if(DCMTK_DOCKER_CONTAINER != null) {
			Path folderFromDcmFile = Paths.get(dcmFile).getParent();
			Path folderFromDestinationFile = Paths.get(destinationFile).getParent();
			
			String dockerCommand = String.format(DCMTK_DOCKER_CONTAINER, folderFromDcmFile + ":" + folderFromDcmFile , folderFromDestinationFile + ":" + folderFromDestinationFile);
			sb.append(dockerCommand);
			sb.append(" dump2dcm -g -q  ");
			sb.append(dcmFile + " ");
			sb.append(destinationFile + " ");
			
			//System.out.println(sb.toString());
		}else {
			sb.append(DicomConfig.getDCMTK_DUMP2DCM_BIN() + " -g -q " + dcmFile + " " + destinationFile);
		}
		
		CommandLine cmdLine = null;
		if (OSValidator.isWindows()) {
			cmdLine = new CommandLine("cmd.exe");

			cmdLine.addArgument("/c");
		} else {
			cmdLine = new CommandLine("/bin/sh");
			cmdLine.addArgument("-c");
		}

		cmdLine.addArgument(sb.toString(), false);

		// System.out.println(cmdLine.toString());

		DefaultExecutor executor = new DefaultExecutor();
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		PumpStreamHandler streamHandler = new PumpStreamHandler(outputStream);
		executor.setStreamHandler(streamHandler);
		//String out = "";

		executor.execute(cmdLine);
		//out = outputStream.toString();
		//if (out.isEmpty() == true)
		
		if(!new File(destinationFile).exists())
			//throw new Exception("File "+ destinationFile +" not generated");
			throw new WorkListNotGeneratedException("File "+ destinationFile +" not generated");

	}
}
